this.Posts = new Mongo.Collection('posts');

Schemas.Posts = new SimpleSchema({
    title: {
        type: String,
        max: 60
    },
    summary: {
        type: String,
        autoform: {
            rows: 3
        }
    },
    content: {
        type: String,
        label: 'Post Content',

        autoform: {
            class: 'editor',
            afFieldInput: {
                rows: 5
            }
        }


    },
    createdAt: {
        type: Date,

        autoValue: function() {
            if (this.val === '') {
                return new Date();
            }
        }
    },
    updatedAt: {
        type: Date,
        optional: true,
        autoValue: function() {
            return new Date();
        }
    },
    owner: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        autoValue: function() {
            if (this.isInsert) {
                return Meteor.userId();
            }
        },
        autoform: {
            options: function() {
                return _.map(Meteor.users.find().fetch(), function(user) {
                    return {
                        label: user.emails[0].address,
                        value: user._id
                    };
                });
            }
        }
    }
});

Posts.attachSchema(Schemas.Posts);