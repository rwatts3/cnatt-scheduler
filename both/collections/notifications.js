
Schemas.Notifications = new SimpleSchema({
    owner: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        autoValue: function () {
            if (this.isInsert) {
                return Meteor.userId();
            }
        },
        autoform: {
            options: function () {
                return _.map(Meteor.users.find().fetch(), function (user) {
                    return {
                        label: user.emails[0].address,
                        value: user._id
                    };
                });
            }
        }
    },
    content: {
        type: String,
        autoform: {
            rows: 5
        }
    },
    link: {
        type: String,
        optional: true
    }
});
Notifications.attachSchema(Schemas.Notifications);