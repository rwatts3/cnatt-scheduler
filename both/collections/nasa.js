  this.Nasa = new Meteor.Collection('nasa');

  Schemas.Nasa = new SimpleSchema({
    shuttlename: {
      type: String,
      max: 60
    },
    numberofcrew: {
      type: Number,
      min: 4
    },
    tags: {
      type: [String],
      autoform: {
        type: 'tags'
      }
    },
    owner: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      autoValue: function() {
        if (this.isInsert) {
          return Meteor.userId();
        }
      },
      autoform: {
        options: function() {
          return _.map(Meteor.users.find().fetch(), function(user) {
            return {
              label: user.emails[0].address,
              value: user._id
            };
          });
        }
      }
    }
  });

  Nasa.attachSchema(Schemas.Nasa);

