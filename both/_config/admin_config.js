var AdminConfig;
AdminConfig = {
    name: Config.name,
    adminEmails: ['ryandwatts@gmail.com'],
    collections: {
        Posts: {
            color: 'teal',
            icon: 'pencil',
            tableColumns: [{
                label: 'Title',
                name: 'title'
            }, {
                label: 'User',
                name: 'owner',
                collection: 'Users'
            }]
        },
        Comments: {
            color: 'aqua',
            icon: 'comments',
            auxCollections: ['Posts'],
            tableColumns: [{
                label: 'Content',
                name: 'content'
            }, {
                label: 'Post',
                name: 'doc',
                collection: 'Posts',
                collection_property: 'title'
            }, {
                label: 'User',
                name: 'owner',
                collection: 'Users'
            }]
        },
        Nasa: {
            color: 'teal',
            icon: 'space-shuttle',
            tableColumns: [{
                label: 'ShuttleName',
                name: 'shuttlename'
            }, {
                label: 'NumberOfCrew',
                name: 'numberofcrew'
            }, {
                label: 'User',
                name: 'owner',
                collection: 'Users'
            }]
        },
        Notifications: {
            color: 'aqua',
            icon: 'bolt',
            omitFields: ['link'],
            tableColumns: [{
                label: 'User',
                name: 'owner',
                collection: 'Users'
            }, {
                label: 'Title',
                name: 'title'
            }, {
                label: 'Content',
                name: 'content'
            }, {
                label: 'Read',
                name: 'read'
            }, {
                label: 'Date',
                name: 'date'
            }, {
                label: 'Icon',
                name: 'icon'
            }, {
                label: 'class',
                name: 'class'
            }]
        }
    },
    Nasa: {
            color: 'teal',
            icon: 'space-shuttle',
            tableColumns: [{
                label: 'ShuttleName',
                name: 'shuttlename'
            }, {
                label: 'NumberOfCrew',
                name: 'numberofcrew'
            }]
        },
    dashboard: {
        homeUrl: '/dashboard'
    }
};
if (Meteor.isClient) {
    window.AdminConfig = AdminConfig;
} else if (Meteor.isServer) {
    global.AdminConfig = AdminConfig;
}