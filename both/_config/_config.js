this.Config = {
    name: 'SandBox',
    title: 'SandBox Title',
    subtitle: 'Sandbox Subtitle',
    version: '0.0.1',
    logo: function () {
        return '<b>' + this.name + '</b>';
    },
    footer: function () {
        return this.name + ' - Copyright ' + new Date().getFullYear();
    },
    emails: {
        from: 'noreply@' + Meteor.absoluteUrl()
    },
    blog: 'http://wattsapplications.com/RyanWatts/',
    about: 'http://wattsapplications.com/RyanWatts/index.cfm/about-me/',
    username: false,
    homeRoute: '/dashboard',
    socialMedia: [['http://twitter.com/RyanDWatts3', 'twitter'], ['https://bitbucket.org/rwatts3', 'bitbucket']]
};

