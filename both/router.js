Router.configure({
    layoutTemplate: "masterLayout",
    loadingTemplate: "loading",
    notFoundTemplate: "notFound",
    routeControllerNameConverter: "camelCase"
});
Router.onBeforeAction("loading");
Router.map(function () {
    this.route("home", {
        path: "/",
        layoutTemplate: "homeLayout"
    });
    this.route("dashboard", {
        path: "/dashboard",
        onBeforeAction: function (pause) {
            if (!Meteor.user()) {
                this.render('entrySignIn');
            } else {
                this.next();
            }
        },
        waitOn: function () {
            return [
                Meteor.subscribe('posts'),
                Meteor.subscribe('favorites'),
                Meteor.subscribe('comments'),
              Meteor.subscribe('nasa')
            ];
        },
        data: function () {
            return {
                Posts: Posts.find({}, {
                    sort: {
                        createdAt: -1
                    }
                }).fetch()
            };
        }
    });
    this.route("profile", {
        path: "/profile",
        waitOn: function () {
            return Meteor.subscribe('profilePictures');
        }
    });
    return this.route("account", {
        path: "/account"
    });
});
Router.waitOn(function () {
    Meteor.subscribe('user');
    return Meteor.subscribe('userPicture');
});