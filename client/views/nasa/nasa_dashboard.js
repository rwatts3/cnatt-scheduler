// Helpers // 
Template.nasaDashboard.helpers({});
Template.nasaSearch.helpers({});
Template.nasaItems.helpers({
    filter: function() {
        var searchFor = Session.get('nasaSearch');
        if (searchFor == '') {
            return Nasa.find({});
        } else {
            return Nasa.find({shuttlename: {$regex: searchFor , $options: 'i'}});
        }
    }
});
// Events // 
Template.nasaDashboard.events({});
Template.nasaSearch.events({
    'keyup #filter': function(e) {
        Session.set('nasaSearch', $('#filter').val());
    }
});
Template.nasaItems.events({});