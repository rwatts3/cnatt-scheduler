AutoForm.hooks({
    updatePassword: {
        onSubmit: function (insertDoc, updateDoc, currentDoc) {
            if (insertDoc["new"] !== insertDoc.confirm) {
                return App.alertError('Passwords do not match');
            }
            Accounts.changePassword(insertDoc.old, insertDoc["new"], function (e) {
                $('.btn-primary').attr('disabled', null);
                if (e) {
                    return App.alertError(e.message);
                } else {
                    return App.alertSuccess('Password Updated');
                }
            });
            return false;
        }
    }
});

