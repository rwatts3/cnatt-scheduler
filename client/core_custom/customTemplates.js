/**
 * This file is used to hijack and overwrite custom logic from packages
 * Put things here like a custom editor "summernote" 
 * Template replacements or editing
 * Custom Template Helpers to add or extend template helpers from the admin package
 */

/**
 * Template Replacements
 */
  Template.myFavoriteButtonFavorited.replaces("favoriteButtonFavorited");

  Template.myFavoriteButtonNotFavorited.replaces("favoriteButtonNotFavorited");

  Template.customNotifications.replaces("notifications");

  Template.customNotificationsDropdown.replaces("notificationsDropdown");

/**
 * Summernote Editor
 */

Template.AdminDashboardNew.rendered = function() {
  initEditor();
};

Template.AdminDashboardEdit.rendered = function() {
  initEditor();
};

function initEditor() {
  $('.editor').summernote({
    // summernote options
  });
};

/**
 * Custom Template Helpers
 * These template helpers are from the package rather than bringing the package in simply overwrite or add template helpers 
 */
// Posts Template Helper
Template.posts.helpers({
	publishedDate : function () {
		var date = this.createdAt;
		var month = date.getUTCMonth() + 1; //months from 1-12
		var day = date.getUTCDate();
		var year = date.getUTCFullYear();

		var formattedDate = year + "/" + month + "/" + day;

		return formattedDate;
	}
});
